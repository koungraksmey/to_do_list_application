package com.example.homeworkspringsecurity1withauthentication;

import com.example.homeworkspringsecurity1withauthentication.model.UserInfo;
import com.example.homeworkspringsecurity1withauthentication.service.UserServiceImp;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

@SpringBootTest
class Homeworkspringsecurity1ApplicationTests {

    @Autowired
    UserServiceImp userServiceImp;

    @Test
    void contextLoads() {
    }


    @Test
    void getUserEmailByEmailOrUserName() {
        UserDetails userDetails = userServiceImp.loadUserByUsername("kaly@gmail.com");
        System.out.println(userDetails);
    }


    @Test
    void getUserFromSecurityContext(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(authentication );
    }





}
