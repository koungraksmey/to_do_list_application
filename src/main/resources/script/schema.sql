create table user_tb
(
    id       serial primary key,
    username varchar(100),
    email    varchar(100),
    password varchar(255)
);


insert into user_tb(username, email, password)
VALUES ('kaly', 'kaly@gmail.com', '$2a$10$jnEozwJHXMaHXphR7SeXCehSMTTJNnPlyWGcg9kZQe.zVpJqIGdLu');
insert into user_tb(username, email, password)
VALUES ('vox', 'vox@gmail.com', '$2a$10$jnEozwJHXMaHXphR7SeXCehSMTTJNnPlyWGcg9kZQe.zVpJqIGdLu');
insert into user_tb(username, email, password)
VALUES ('billy', 'billy@gmail.com', '$2a$10$jnEozwJHXMaHXphR7SeXCehSMTTJNnPlyWGcg9kZQe.zVpJqIGdLu');
insert into user_tb(username, email, password)
VALUES ('bro2', 'bro2@gmail.com', '$2a$10$jnEozwJHXMaHXphR7SeXCehSMTTJNnPlyWGcg9kZQe.zVpJqIGdLu')
returning *;


create table role_tb
(
    id   serial primary key,
    name varchar(25)
);


-- ⚠️⚠️⚠️ Teacher, please make sure that user role is in order
-- ROLE_USER has primary key number 1 and ROLE_ADMIN has primary key number

insert into role_tb(name)
values ('ROLE_USER');
insert into role_tb(name)
values ('ROLE_ADMIN');

-- 🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏🙏



create table user_roles_tb
(
    user_id int references user_tb (id) on DELETE cascade on update cascade,
    role_id int references role_tb (id) on delete cascade on update cascade
);
insert into user_roles_tb(user_id, role_id)
values (1, 1);
insert into user_roles_tb(user_id, role_id)
values (1, 2);
insert into user_roles_tb(user_id, role_id)
values (2, 2);
insert into user_roles_tb(user_id, role_id)
values (3, 1);

insert into user_roles_tb(user_id, role_id)
values (4, 1);
insert into user_roles_tb(user_id, role_id)
values (4, 2);
insert into user_roles_tb(user_id, role_id)
values (3, 2);
insert into user_roles_tb(user_id, role_id)
values (3, 1);


drop table user_roles_tb;


-- create table task_tb
create table task_tb
(
    id          serial primary key,
    name        varchar(255),
    description varchar(500),
    date        timestamp default current_timestamp,
    status      varchar(100),
    user_id     int references user_tb (id) on delete cascade on update cascade,
    category_id int references category_tb (id) on delete cascade on update cascade

);

create table category_tb
(
    id      serial primary key,
    name    varchar(100),
    date    timestamp default current_timestamp,
    user_id int references user_tb (id) on delete cascade on update cascade
);


-- test join
select u.*, rt.name
from user_tb u
         inner join user_roles_tb urt on u.id = urt.user_id
         inner join role_tb rt on urt.role_id = rt.id;


-- get user role name by user id
select rt.name
from role_tb rt
         inner join user_roles_tb urt on rt.id = urt.role_id
where user_id = 4;


-- get user by email or username
select *
from user_tb;
select *
from user_tb
where email = 'kaly@gmail.com'
   or username = 'kaly';


delete
from user_tb
where id = 3;


-- test task table
insert into task_tb (name, description, date, status, user_id, category_id)
values ('ttt', 'ddddd','2023-03-27T12:40:57.797Z', 'inprocess', 1, 1)
returning *;


select * from task_tb;
--     get task by id;

select * from task_tb where id = 1;


--test category table
insert into category_tb (name, user_id) values('Go to svayreang', 1) returning *;
select id from category_tb;



-- filter task by status for current user

select * from task_tb where status = 'is_in_review' and user_id = 2;



-- get all task for current user id
select * from task_tb where user_id = 2;


-- update task by taskId for current user
update task_tb set name = 'tttt', description = 'sdfsd' where id = 1 and user_id = 2;


--



