package com.example.homeworkspringsecurity1withauthentication.secrityConfig;


import com.example.homeworkspringsecurity1withauthentication.service.UserServiceImp;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@RequiredArgsConstructor
//@AllArgsConstructor
public class JwtSecurityConfig {
    private final BCryptPasswordEncoder passwordEncoder;
    private final UserServiceImp userServiceImp;

    private final JwtAuthenticationFilter jwtAuthenticationFilter;



    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider =
                new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userServiceImp);
        authenticationProvider.setPasswordEncoder(passwordEncoder);
        return authenticationProvider;
    }


    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }


        @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
                .csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeHttpRequests()

                .requestMatchers("/api/v1/users/user").hasRole("USER")
                .requestMatchers("/api/v1/users/admin").hasRole("ADMIN")
                .requestMatchers("/api/v1/users/user_admin").hasAnyRole("USER", "ADMIN")
                .requestMatchers("/v3/api-docs/**", "/swagger-ui/**",
                        "/swagger-ui.html", "/api/v1/auth/**", "/api/v1/users/home", "/api/v1/users/register").permitAll()
//                .requestMatchers("/api/v1/categories", "/api/v1/categories/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .authenticationProvider(authenticationProvider())
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .formLogin()
                .and()
                .httpBasic()
                .and()
                .logout()
                .and()
                .build();

    }
}

