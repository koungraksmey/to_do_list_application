//package com.example.homeworkspringsecurity1withauthentication.secrityConfig;
//
//
//import com.example.homeworkspringsecurity1withauthentication.service.UserServiceImp;
//import lombok.AllArgsConstructor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.web.SecurityFilterChain;
//
//@Configuration
//@AllArgsConstructor
//public class DatabaseSecurityConfig {
//
//    private final UserServiceImp userServiceImp;
//    private final BCryptPasswordEncoder bCryptPasswordEncoder;
//
//
//    @Bean
//    public DaoAuthenticationProvider daoAuthenticationProvider() {
//        DaoAuthenticationProvider daoAuthenticationProvider
//                = new DaoAuthenticationProvider();
//        daoAuthenticationProvider.setUserDetailsService(userServiceImp);
//        daoAuthenticationProvider.setPasswordEncoder(bCryptPasswordEncoder);
//        return daoAuthenticationProvider;
//    }
//
//
//    @Bean
//    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
//        return httpSecurity
//                .csrf().disable()
//                .authorizeHttpRequests()
//
//                .requestMatchers("/api/v1/users/user").hasRole("USER")
//                .requestMatchers("/api/v1/users/admin").hasRole("ADMIN")
//                .requestMatchers("/api/v1/users/user_admin").hasAnyRole("USER", "ADMIN")
//                .requestMatchers("/v3/api-docs/**", "/swagger-ui/**",
//                        "/swagger-ui.html","/api/v1/users/register", "/api/v1/users/home").permitAll()
//
//                .anyRequest().authenticated()
//                .and()
//                .formLogin()
//                .and()
//                .httpBasic()
//                .and()
//                .build();
//
//    }
//}
