package com.example.homeworkspringsecurity1withauthentication.secrityConfig;


import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class InMemoryUserConfig {

    private final BCryptPasswordEncoder passwordEncoder;

//    @Bean
//    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
//        return httpSecurity
//                .csrf().disable()
//                .authorizeHttpRequests()
//
//                .requestMatchers("/api/v1/users/user").hasRole("USER")
//                .requestMatchers("/api/v1/users/admin").hasRole("ADMIN")
//                .requestMatchers("/api/v1/users/user_admin").hasAnyRole("USER", "ADMIN")
//                .requestMatchers("/api/v1/users/home").permitAll()
//                .anyRequest().authenticated()
//                .and()
//                .formLogin().disable()
////                .and()
//                .httpBasic()
//                .and()
//                .build();
//
//    }

//    @Bean
//    public UserDetailsService userDetailsService() {
//        UserDetails user = User.withUsername("user")
//                .password(passwordEncoder.encode("123"))
//                .roles("USER")
//                .build();
//
//        UserDetails admin = User.withUsername("admin")
//                .password(passwordEncoder.encode("123"))
//                .roles("ADMIN")
//                .build();
//        return new InMemoryUserDetailsManager(user, admin);
//    }
}
