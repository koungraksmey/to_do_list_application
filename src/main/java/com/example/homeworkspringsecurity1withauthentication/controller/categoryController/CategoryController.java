package com.example.homeworkspringsecurity1withauthentication.controller.categoryController;


import com.example.homeworkspringsecurity1withauthentication.model.UserInfo;
import com.example.homeworkspringsecurity1withauthentication.model.categoryModel.Category;
import com.example.homeworkspringsecurity1withauthentication.model.categoryModel.CategoryRequest;
import com.example.homeworkspringsecurity1withauthentication.model.response.CustomResponse;
import com.example.homeworkspringsecurity1withauthentication.service.categoryService.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
@SecurityRequirement(name = "bearerAuth")
@AllArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;


    @PostMapping
    @Operation(summary = "Save new category for current user")
    public ResponseEntity<CustomResponse<Category>> addNewCategory(@RequestBody CategoryRequest categoryRequest){
        UsernamePasswordAuthenticationToken token =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) token.getPrincipal();
        int userId = userInfo.getUserId();

        Integer storeCategoryId = categoryService.addNewCategory(categoryRequest,userId);
        if(storeCategoryId != null){
            CustomResponse<Category> response = CustomResponse.<Category>builder()
                    .message("Add new category successfully")
                    .payload(categoryService.getCategoryById(storeCategoryId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @GetMapping()
    @Operation(summary = "Get all category")
    public ResponseEntity<CustomResponse<List<Category>>> getAllCategory(){
        CustomResponse<List<Category>> response = CustomResponse.<List<Category>>builder()
                .message("Get all category successfully")
                .payload(categoryService.getAllCategory())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get category by Id")
    public ResponseEntity<CustomResponse<Category>> getCategoryById(@PathVariable("id") Integer categoryId){
        CustomResponse<Category> response;
        response = CustomResponse.<Category>builder()
                .message("Get category by id successfully!")
                .payload(categoryService.getCategoryById(categoryId))
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/users")
    @Operation(summary = "Get all category for current user")
    public ResponseEntity<CustomResponse<List<Category>>> getCategoryForCurrentUser(
            @RequestParam(defaultValue = "false") Boolean asc,
            @RequestParam(defaultValue = "false") Boolean desc,
            @RequestParam(defaultValue = "1") Integer pages,
            @RequestParam(defaultValue = "10") Integer size){

        CustomResponse<List<Category>> response;

        UsernamePasswordAuthenticationToken token =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) token.getPrincipal();
        int userId = userInfo.getUserId();

        response = CustomResponse.<List<Category>>builder()
                .message("Get all category for current user successfully!")
                .payload(categoryService.getAllCategoryForCurrentUser(userId, asc, desc, pages, size))
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);

    }

    @GetMapping("/{id}/users")
    @Operation(summary = "Get category by Id for current user")
    public ResponseEntity<CustomResponse<Category>> getCategoryByIdForCurrentUser(@PathVariable("id") Integer categoryId){

        CustomResponse<Category> response;

        UsernamePasswordAuthenticationToken token =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) token.getPrincipal();
        int userId = userInfo.getUserId();

        response = CustomResponse.<Category>builder()
                .message("Get category by id for current user successfully!")
                .payload(categoryService.getCategoryByIdForCurrentUser(categoryId,userId))
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}/users")
    @Operation(summary = "Delete category by id for current user")
    public ResponseEntity<CustomResponse<String>> deleteCategoryById(@PathVariable("id") Integer categoryId){
        CustomResponse<String> response;
        UsernamePasswordAuthenticationToken token =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) token.getPrincipal();
        int userId = userInfo.getUserId();
        if(categoryService.deleteCategoryByIdForCurrentUser(categoryId,userId)){
            response = CustomResponse.<String>builder()
                    .message("Delete category successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            return ResponseEntity.notFound().build();
        }

    }

    @PutMapping("/{id}")
    @Operation(summary = "Update category by Id for current user")
    public ResponseEntity<CustomResponse<Category>> updateCategoryByIdForCurrentUser(
            @RequestBody CategoryRequest categoryRequest, @PathVariable("id") Integer categoryId
    ){
        CustomResponse<Category> response;

        UsernamePasswordAuthenticationToken token =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) token.getPrincipal();
        int userId = userInfo.getUserId();

        Integer idCategory = categoryService.updateCategoryForCurrentUser(categoryRequest,categoryId,userId);
        if(idCategory != null){
            response = CustomResponse.<Category>builder()
                    .message("Updated category by Id Successfully!")
                    .payload(categoryService.getCategoryById(idCategory))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }else {
            return ResponseEntity.notFound().build();
        }
    }

}
