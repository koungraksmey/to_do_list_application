package com.example.homeworkspringsecurity1withauthentication.controller;


import com.example.homeworkspringsecurity1withauthentication.model.UserInfo;
import com.example.homeworkspringsecurity1withauthentication.model.UserRequest;
import com.example.homeworkspringsecurity1withauthentication.service.UserServiceImp;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
@AllArgsConstructor
public class RegisterController {

    private final UserServiceImp userServiceImp;


    @PostMapping("/register")
    public String addUser(@RequestBody UserRequest userRequest) {
        System.out.println(userRequest);
        UserInfo userInfo = userServiceImp.addNewUser(userRequest);
        System.out.println(userInfo);
        if(userInfo != null) {
            return "Register successfully";
        }

        return null;
    }


}
