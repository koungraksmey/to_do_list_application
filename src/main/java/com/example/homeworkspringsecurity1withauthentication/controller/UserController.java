package com.example.homeworkspringsecurity1withauthentication.controller;


import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
@SecurityRequirement(name = "bearerAuth")
@Hidden
public class UserController {


    @GetMapping("/home")
    public String getHome() {
        return "Hello from home page";
    }


    @GetMapping("/user")
    public String getUser() {
        return "Hello from user page";
    }


    @GetMapping("/admin")
    public String getAdmin() {
        return "Hello from admin page";
    }


    @GetMapping("/user_admin")
    public String getUserOrAdmin() {
        return "Hello from User or Admin page";
    }
}
