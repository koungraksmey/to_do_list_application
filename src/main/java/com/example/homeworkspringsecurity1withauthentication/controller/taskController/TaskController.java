package com.example.homeworkspringsecurity1withauthentication.controller.taskController;


import com.example.homeworkspringsecurity1withauthentication.exception.taskexception.CategoryNotFoundException;
import com.example.homeworkspringsecurity1withauthentication.exception.taskexception.ResourceNotFoundException;
import com.example.homeworkspringsecurity1withauthentication.exception.taskexception.WrongFormatInputTaskException;
import com.example.homeworkspringsecurity1withauthentication.exception.taskexception.YouAreNotOwnerException;
import com.example.homeworkspringsecurity1withauthentication.model.UserInfo;
import com.example.homeworkspringsecurity1withauthentication.model.taskModel.*;
import com.example.homeworkspringsecurity1withauthentication.repository.CategoryRepository;
import com.example.homeworkspringsecurity1withauthentication.service.taskService.TaksService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.ibatis.annotations.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.*;

@RestController
@RequestMapping("/api/v1/tasks")
@SecurityRequirement(name = "bearerAuth")
@AllArgsConstructor
public class TaskController {

    private final TaksService taskService;
    private final CategoryRepository categoryRepository;


    @GetMapping("/{id}/users")
    @Operation(summary = "Get task by id for current user.")
    public ResponseEntity<?> getTaskByIdForCurrentUser(@Param("id") Integer taskId) throws YouAreNotOwnerException {

        UsernamePasswordAuthenticationToken token =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) token.getPrincipal();
        int userId = userInfo.getUserId();


        Task taskByIdForCurrentUser = taskService.getTaskByIdForCurrentUser(taskId, userId);

        if (taskByIdForCurrentUser == null) {
            throw new YouAreNotOwnerException("You are not the owner of this task with id: " + taskId);
        }


        TaskResponseGeneric<Task> taskResponseGeneric =
                TaskResponseGeneric.<Task>builder()
                        .payload(taskByIdForCurrentUser)
                        .date(new Timestamp(System.currentTimeMillis()))
                        .success("true")
                        .build();

        return ResponseEntity.ok(taskResponseGeneric);

    }


    @PostMapping("/users")
    @Operation(summary = "Add new task")
    public ResponseEntity<?> addTask(@RequestBody @Valid TaskRequest taskRequest) throws WrongFormatInputTaskException, CategoryNotFoundException {
        UsernamePasswordAuthenticationToken token =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) token.getPrincipal();
        int userId = userInfo.getUserId();

//task validation
        taskValidation(taskRequest);
        validateCategoryIsExistInApp(taskRequest);


        Task task = Task.builder()
                .taskName(taskRequest.getTaskName())
                .description(taskRequest.getDescription())
                .timestamp(taskRequest.getDate())
                .categoryId(taskRequest.getCategoryId())
                .userId(userId)
                .status(taskRequest.getStatus())
                .build();


        Task savedTask = taskService.addTask(task);

        Map<String, Object> taskResponse = new LinkedHashMap<>();
        taskResponse.put("payload", savedTask);
        taskResponse.put("date", new Timestamp(System.currentTimeMillis()));
        taskResponse.put("success", "true");

        return ResponseEntity.ok(taskResponse);
    }


    @GetMapping("")
    @Operation(summary = "Get All task")
    public ResponseEntity<TaskResponse> getAllTasks() {
        List<Task> taskResponseList = taskService.getAllTask();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        TaskResponse taskResponse =
                TaskResponse.builder()
                        .payload(taskResponseList)
                        .date(timestamp)
                        .success("true")
                        .build();
        return ResponseEntity.ok(taskResponse);

    }


    @GetMapping("/{taskId}")
    @Operation(summary = "Get task by id")
    public ResponseEntity<?> getTaskById(@PathVariable("taskId") Integer taskId) {
        Task task = taskService.getTaskById(taskId);
        Map<String, Object> returnTask = new LinkedHashMap<>();
        returnTask.put("payload", task);
        returnTask.put("success", "true");
        returnTask.put("date", new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(returnTask);
    }


    @GetMapping("/status/users")
    @Operation(summary = "Filter task by status for current user")
    public ResponseEntity<?> filterTaskByStatusForCurrentUser(@RequestParam(value = "status", required = true) String status) throws CategoryNotFoundException, WrongFormatInputTaskException {
        UsernamePasswordAuthenticationToken token =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) token.getPrincipal();
        int userId = userInfo.getUserId();
//        System.out.println("See: " + userId);


//        validate status
        TaskRequest taskRequest = new TaskRequest();
        taskRequest.setStatus(status);
        taskValidation(taskRequest);


        List<Task> listOfTaskForCurrentUser = taskService.filterTaskByStatusForCurrentCuser(status, userId);
        if (listOfTaskForCurrentUser == null || listOfTaskForCurrentUser.isEmpty()) {
            throw new ResourceNotFoundException("Resource is not found.");
        }


        Map<String, Object> retrievedTasksOfCurrentUser =
                new LinkedHashMap<>();
        retrievedTasksOfCurrentUser.put("payload", listOfTaskForCurrentUser);
        retrievedTasksOfCurrentUser.put("date", new Timestamp(System.currentTimeMillis()));
        retrievedTasksOfCurrentUser.put("success", "true");


        return ResponseEntity.ok(retrievedTasksOfCurrentUser);
    }

    @Data
    static class Status {
        String status;
    }

    @GetMapping("/users")
    @Operation(summary = "Get all tasks for current user id.")
    public ResponseEntity<TaskResponseGeneric<List<Task>>> getAllTaskForCurrentUserId(
            @RequestParam(defaultValue = "false") Boolean asc,
            @RequestParam(defaultValue = "false") Boolean desc,
            @RequestParam(defaultValue = "1") Integer pages,
            @RequestParam(defaultValue = "10") Integer size
    ) {
        UsernamePasswordAuthenticationToken token =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) token.getPrincipal();
        int userId = userInfo.getUserId();
        List<Task> taskListForCurrentUser = taskService.getAllTasksForCurrentUser(userId, asc, desc, pages, size);

        TaskResponseGeneric<List<Task>> taskResponseGeneric =
                TaskResponseGeneric.<List<Task>>builder()
                        .date(new Timestamp(System.currentTimeMillis()))
                        .success("true")
                        .payload(taskListForCurrentUser)
                        .build();
        return ResponseEntity.ok(taskResponseGeneric);
    }


    private void taskValidation(TaskRequest taskRequest) throws WrongFormatInputTaskException {
        String extractTask = taskRequest.getStatus();
        for (String task : TaskConstant.getAllTaskStatus()) {
            if (extractTask.equalsIgnoreCase(task)) {
                return;
            }
        }
        throw new WrongFormatInputTaskException("Invalid task status.");
    }


    private void validateCategoryIsExistInApp(TaskRequest taskRequest) throws CategoryNotFoundException {
        List<Integer> allCateIdIsExistInApp = categoryRepository.getAllCategoryIdExistInApp();
        for (Integer n : allCateIdIsExistInApp) {
            if (Objects.equals(n, taskRequest.getCategoryId())) {
                return;
            }
        }

        throw new CategoryNotFoundException("Invalid category with id: " + taskRequest.getCategoryId());

    }


    @DeleteMapping("/{taskId}/users")
    public ResponseEntity<TaskResponseGeneric<Task>> deleteTaskByIdForCurrentUser(@PathVariable("taskId") Integer taskId) throws YouAreNotOwnerException {


        UsernamePasswordAuthenticationToken token =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) token.getPrincipal();
        int userId = userInfo.getUserId();


        Task isTaskFound = taskService.deleteTaskByIdForCurrent(taskId, userId);


        TaskResponseGeneric<Task> taskResponse =
                TaskResponseGeneric.<Task>builder()
                        .payload(isTaskFound)
                        .date(new Timestamp(System.currentTimeMillis()))
                        .success("true")
                        .build();

        return ResponseEntity.ok(taskResponse);


    }


    @PutMapping("/{taskId}/users")
    public ResponseEntity<TaskResponseGeneric<?>> updateTaskByIdForCurrentUser(@RequestBody TaskRequest taskRequest, @PathVariable("taskId") Integer taskId) throws YouAreNotOwnerException {

        UsernamePasswordAuthenticationToken token =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) token.getPrincipal();
        Integer userId = userInfo.getUserId();


        Task task = taskService.updateTaskbyIdForCurrentUser(taskRequest, taskId, userId);
        TaskResponseGeneric<Task> taskResponseGeneric =
                TaskResponseGeneric.<Task>builder()
                        .payload(task)
                        .date(new Timestamp(System.currentTimeMillis()))
                        .success("true")
                        .build();
        return ResponseEntity.ok(taskResponseGeneric);
    }

}
