package com.example.homeworkspringsecurity1withauthentication.service.categoryService;

import com.example.homeworkspringsecurity1withauthentication.exceptionCategory.CheckException;
import com.example.homeworkspringsecurity1withauthentication.exceptionCategory.IdNotFoundException;
import com.example.homeworkspringsecurity1withauthentication.model.categoryModel.Category;
import com.example.homeworkspringsecurity1withauthentication.model.categoryModel.CategoryRequest;
import com.example.homeworkspringsecurity1withauthentication.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public Integer addNewCategory(CategoryRequest categoryRequest, Integer userId) {
        if((categoryRequest.getCategoryName().trim().isEmpty())){
            throw new CheckException("field can not empty");
        }else {
            Integer getCategoryId = categoryRepository.saveCategory(categoryRequest,userId);
            return getCategoryId;
        }
    }

    @Override
    public List<Category> getAllCategory() {
        List<Category>  category = categoryRepository.findAllCategory();
        if (category.size() > 0 ) {
            return category;
        }
        throw new CheckException("No Data!!!");
    }

    @Override
    public Category getCategoryById(Integer categoryId) {
        if(categoryId<=0 || categoryId.toString().matches("^([+-]?(\\\\d+\\\\.)?\\\\d+)$")){
            throw new CheckException("Input invalid ID");
        }else {
            Category category = categoryRepository.getCategoryById(categoryId);
            if (category == null) {
                throw new IdNotFoundException("category", categoryId);
            } else return category;
        }
    }

    @Override
    public List<Category> getAllCategoryForCurrentUser(Integer userId, boolean asc, boolean desc, Integer page, Integer size) {
        List<Category> categoryForCurrentUser = categoryRepository.getAllCategoryForCurrentUser(userId, asc, desc, page, size);
        if (categoryForCurrentUser.size() > 0 ) {
            return categoryForCurrentUser;
        }
        throw new CheckException("No Data!!!");
    }
    @Override
    public Category getCategoryByIdForCurrentUser(Integer categoryId, Integer userId) {
        if(categoryId<=0 || categoryId.toString().matches("^([+-]?(\\\\d+\\\\.)?\\\\d+)$")){
            throw new CheckException("Input invalid ID");
        }else{
            Category categoryByIdForCurrentUser = categoryRepository.getCategoryByIdForCurrentUser(categoryId,userId);
            Category category = categoryRepository.getCategoryById(categoryId);
            if (category == null && categoryByIdForCurrentUser == null) {
                throw new IdNotFoundException("Category", categoryId);
            }if (category != null && categoryByIdForCurrentUser == null) {
                throw new CheckException("Cannot get this category because you are not the owner!!!");
            }
            else return category;
        }
    }

    @Override
    public boolean deleteCategoryByIdForCurrentUser(Integer categoryId, Integer userId) {
        if(categoryId<=0 || categoryId.toString().matches("^([+-]?(\\\\d+\\\\.)?\\\\d+)$")){
            throw new CheckException("Input invalid ID");
        }else {
            boolean isDeleteCategoryById = categoryRepository.deleteCategoryByIdForCurrentUser(categoryId,userId);
            Category category = categoryRepository.getCategoryById(categoryId);
            if(!isDeleteCategoryById  && category == null){
                throw new IdNotFoundException("Category",categoryId);

            }else if(!isDeleteCategoryById  && category != null){
                throw new CheckException("Cannot delete this category because you are not the owner!!!");
            }else {
                return isDeleteCategoryById;
            }
        }
    }

    @Override
    public Integer updateCategoryForCurrentUser(CategoryRequest categoryRequest, Integer categoryId, Integer userId) {
        if(categoryId<=0 || categoryId.toString().matches("^([+-]?(\\\\d+\\\\.)?\\\\d+)$")){
            throw new CheckException("Input invalid ID");
        }else {
            Category getCategoryByIdForCurrentUser = categoryRepository.getCategoryByIdForCurrentUser(categoryId,userId);
            Category getCategoryById = categoryRepository.getCategoryById(categoryId);
            if(getCategoryById == null && getCategoryByIdForCurrentUser == null){
                throw new IdNotFoundException("category",categoryId);
            } else if (getCategoryById != null && getCategoryByIdForCurrentUser == null) {
                throw new CheckException("Cannot update this category because you are not the owner!!!");
            } else if(categoryRequest.getCategoryName().trim().isEmpty() || categoryRequest.getCategoryName().equals("string") ){
                throw new CheckException("field can not empty or string");
            }else {
                Integer updateCategoryId = categoryRepository.updateCategory(categoryRequest, categoryId,userId);
                return updateCategoryId;
            }
        }

    }

}
