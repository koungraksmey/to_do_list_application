package com.example.homeworkspringsecurity1withauthentication.service.categoryService;

import com.example.homeworkspringsecurity1withauthentication.model.categoryModel.Category;
import com.example.homeworkspringsecurity1withauthentication.model.categoryModel.CategoryRequest;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CategoryService {

    Integer addNewCategory(CategoryRequest categoryRequest,Integer userId);
    List<Category> getAllCategory();

    Category getCategoryById (Integer categoryId);
    List<Category> getAllCategoryForCurrentUser (Integer userId, boolean asc, boolean desc, Integer page, Integer size);

    Category getCategoryByIdForCurrentUser (Integer categoryId, Integer userId);
    boolean deleteCategoryByIdForCurrentUser(Integer categoryId,Integer userId);

    Integer updateCategoryForCurrentUser (CategoryRequest categoryRequest, Integer categoryId, Integer userId);
}
