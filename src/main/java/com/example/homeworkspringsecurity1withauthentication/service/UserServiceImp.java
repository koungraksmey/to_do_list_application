package com.example.homeworkspringsecurity1withauthentication.service;

import com.example.homeworkspringsecurity1withauthentication.model.Role;
import com.example.homeworkspringsecurity1withauthentication.model.UserInfo;
import com.example.homeworkspringsecurity1withauthentication.model.UserRequest;
import com.example.homeworkspringsecurity1withauthentication.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class UserServiceImp implements UserDetailsService {

    private final UserRepository userRepository;


    private final ModelMapper modelMapper;

    private final BCryptPasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserInfo userInfo = userRepository.findByEmail(email);
        if(userInfo == null) {
            throw new UsernameNotFoundException("user not found.");
        }
        System.out.println(userInfo);
        return userInfo;
    }


    public UserInfo addNewUser(UserRequest userRequest) {
        boolean isRoole = false;
        UserInfo userInfo = modelMapper.map(userRequest, UserInfo.class);
        System.out.println(userInfo);
        userInfo.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        for (Role role : Role.values()) {

            for(String r: userInfo.getRoles()){
                if (r.equalsIgnoreCase(role.name())) {
//                    userInfo.getRoles().add(r);
                    isRoole = true;
                    break;
                }
            }

        }
        if(!isRoole) {
            throw new RuntimeException();
        }

        return userRepository.saveUserMain(userInfo);

    }
}
