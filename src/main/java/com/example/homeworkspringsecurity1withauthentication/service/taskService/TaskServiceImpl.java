package com.example.homeworkspringsecurity1withauthentication.service.taskService;

import com.example.homeworkspringsecurity1withauthentication.exception.taskexception.ResourceNotFoundException;
import com.example.homeworkspringsecurity1withauthentication.exception.taskexception.YouAreNotOwnerException;
import com.example.homeworkspringsecurity1withauthentication.model.taskModel.Task;
import com.example.homeworkspringsecurity1withauthentication.model.taskModel.TaskRequest;
import com.example.homeworkspringsecurity1withauthentication.repository.CategoryRepository;
import com.example.homeworkspringsecurity1withauthentication.repository.TaskRepsoitory;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;


@Service
@AllArgsConstructor
public class TaskServiceImpl implements TaksService {

    private final TaskRepsoitory taskRepsoitory;
    private final CategoryRepository categoryRepository;

    @Override
    public Task addTask(Task task) {
        return taskRepsoitory.addTask(task);
    }

    @Override
    public List<Task> getAllTask() {
        return taskRepsoitory.getAllTask();
    }

    @Override
    public Task getTaskById(Integer taskId) {
        Task task = taskRepsoitory.getTaskById(taskId);
        if (task == null) {
            throw new ResourceNotFoundException("Task not found with id: " + taskId);
        }
        return task;
    }


    @Override
    public List<Task> filterTaskByStatusForCurrentCuser(String status, Integer userId) {
        return taskRepsoitory.filterTaskByStatusForCurrent(status, userId);
    }

    @Override
    public List<Task> getAllTasksForCurrentUser(Integer currentUserId, boolean asc, boolean desc, Integer page, Integer size) {
        return taskRepsoitory.getAllTasksForCurrentUser(currentUserId, asc, desc, page, size);
    }


    @Override
    public Task getTaskByIdForCurrentUser(Integer taskId, Integer userId) {
        Task isTaskFound = getTaskById(taskId);
        return taskRepsoitory.getTaskByIdForCurrentUser(taskId, userId);
    }

    @Override
    public Task deleteTaskByIdForCurrent(Integer taskId, Integer currentUserId) throws YouAreNotOwnerException {
        Task isTaskFound = getTaskById(taskId);
        if(!Objects.equals(isTaskFound.getUserId(), currentUserId)) {
            throw new YouAreNotOwnerException("You are not the owner of task with id: " + taskId);
        }
        return taskRepsoitory.deleteTaskByIdForCurrent(taskId, currentUserId);
    }


    @Override
    public Task updateTaskbyIdForCurrentUser(TaskRequest taskRequest, Integer taskId, Integer userId) throws YouAreNotOwnerException {
        Task task = getTaskById(taskId);

        System.out.println(task);

        List<Integer> categoryListIdNumber = categoryRepository.getAllCategoryIdExistInApp();

        boolean isCategoryFound = false;
        for (Integer id : categoryListIdNumber) {
            if (Objects.equals(id, taskRequest.getCategoryId())) {
                isCategoryFound = true;
                break;
            }
        }

        if(!isCategoryFound){
            throw new ResourceNotFoundException("Category with id: " + taskRequest.getCategoryId() + " not found.");
        }


        if (!Objects.equals(task.getUserId(), userId)) {
            throw new YouAreNotOwnerException("You are not own of task.");
        }

        return taskRepsoitory.updateTaskbyIdForCurrentUser(taskRequest, taskId, userId);



    }
}
