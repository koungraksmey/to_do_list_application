package com.example.homeworkspringsecurity1withauthentication.service.taskService;

import com.example.homeworkspringsecurity1withauthentication.exception.taskexception.YouAreNotOwnerException;
import com.example.homeworkspringsecurity1withauthentication.model.taskModel.Task;
import com.example.homeworkspringsecurity1withauthentication.model.taskModel.TaskRequest;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TaksService {
    Task addTask(Task task);
    List<Task> getAllTask();
    Task getTaskById(@Param("taskId") Integer taskId);

    List<Task> filterTaskByStatusForCurrentCuser(String status,Integer userId);

    List<Task> getAllTasksForCurrentUser(Integer currentUserId, boolean asc, boolean desc, Integer page, Integer size);

    Task getTaskByIdForCurrentUser(Integer taskId,  Integer userId);


    Task deleteTaskByIdForCurrent( Integer taskId, Integer currentUserId) throws YouAreNotOwnerException;


    Task updateTaskbyIdForCurrentUser( TaskRequest taskRequest, Integer taskId, Integer userId) throws YouAreNotOwnerException;
}
