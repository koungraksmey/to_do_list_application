package com.example.homeworkspringsecurity1withauthentication.model;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}
