package com.example.homeworkspringsecurity1withauthentication.model.taskModel;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;

@Builder
@Data
@AllArgsConstructor
public class TaskResponseGeneric<T> {

    private T payload;
    private String success;
    private Timestamp date;
}
