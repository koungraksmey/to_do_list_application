package com.example.homeworkspringsecurity1withauthentication.model.taskModel;

import java.util.List;

public class TaskConstant {
    public static String IS_IN_PROCESS = "is_in_process";
    public static String IS_COMPLETE = "is_complete";
    public static String IS_IN_REVIEW = "is_in_review";
    public static String IS_CANCELLED = "is_cancelled";


    public static List<String> getAllTaskStatus() {
        return List.of(IS_IN_PROCESS, IS_COMPLETE, IS_IN_REVIEW, IS_CANCELLED);
    }

}
