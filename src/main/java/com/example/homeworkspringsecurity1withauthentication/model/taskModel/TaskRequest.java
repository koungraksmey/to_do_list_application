package com.example.homeworkspringsecurity1withauthentication.model.taskModel;


import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskRequest {

    @NotBlank(message = "task name must be provided")
    private String taskName;

    @NotBlank(message = "description must be provided")
    private String description;
    private Timestamp date;

    @NotBlank(message = "status must be provide")
    private String status;


    @Min(value = 1, message = "category id out of rank")
    private Integer categoryId;

}
