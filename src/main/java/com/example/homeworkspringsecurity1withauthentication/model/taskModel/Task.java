package com.example.homeworkspringsecurity1withauthentication.model.taskModel;


import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Task {
    private Integer taskId;
    private String taskName;
    private String description;
    private Timestamp timestamp;
    private String status;

    private Integer userId;
    private Integer categoryId;

}
