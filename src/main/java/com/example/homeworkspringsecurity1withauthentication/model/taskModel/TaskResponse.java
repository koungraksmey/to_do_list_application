package com.example.homeworkspringsecurity1withauthentication.model.taskModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.catalina.LifecycleState;

import java.sql.Timestamp;
import java.util.List;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskResponse {

    private List<Task> payload;
    Timestamp date;

    String success;
}
