package com.example.homeworkspringsecurity1withauthentication.model.categoryModel;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Category {

    Integer categoryId;
    String categoryName;

    Timestamp timestamp;

    Integer userId;

}
