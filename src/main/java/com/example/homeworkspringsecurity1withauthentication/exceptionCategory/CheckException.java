package com.example.homeworkspringsecurity1withauthentication.exceptionCategory;

public class CheckException extends RuntimeException{
    public CheckException(String message){
        super(message);
    }
}
