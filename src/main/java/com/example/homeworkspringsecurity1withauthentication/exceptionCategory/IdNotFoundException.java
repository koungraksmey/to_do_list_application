package com.example.homeworkspringsecurity1withauthentication.exceptionCategory;

public class IdNotFoundException extends RuntimeException{
    public IdNotFoundException(String type, Integer id){
        super(type+" with id : "+ id + " not found");
    }


}
