package com.example.homeworkspringsecurity1withauthentication.repository;


import com.example.homeworkspringsecurity1withauthentication.model.taskModel.Task;
import com.example.homeworkspringsecurity1withauthentication.model.taskModel.TaskRequest;
import com.example.homeworkspringsecurity1withauthentication.model.taskModel.TaskResponse;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface TaskRepsoitory {
    @Select("""
            insert into task_tb (name, description, date, status,user_id, category_id)
            values (#{task.taskName}, #{task.description},#{task.timestamp}, #{task.status},#{task.userId}, #{task.categoryId}) returning *
            """)
    @Results(id = "m1", value = {
            @Result(property = "taskId", column = "id"),
            @Result(property = "taskName", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "timestamp", column = "date"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "categoryId", column = "category_id"),

    })
    Task addTask(@Param("task") Task task);


    //    author RAKSMEY
    @Select("""
            select * from task_tb
            """)

    @ResultMap("m1")
    List<Task> getAllTask();


    @Select("""
            select * from task_tb where id = #{taskId}
            """)
    @ResultMap("m1")
    Task getTaskById(@Param("taskId") Integer taskId);


    @Select("""
            select * from task_tb where status = #{status} and user_id = #{userId}
            """)
    @ResultMap("m1")
    List<Task> filterTaskByStatusForCurrent(@Param("status") String status, @Param("userId") Integer userId);


    @Select("""
            select * from task_tb where user_id = #{currentUserId} ORDER BY 
            CASE WHEN #{asc} THEN date END ASC,
            CASE WHEN #{desc} THEN date END DESC 
            LIMIT #{size} OFFSET #{size} * (#{page} - 1) 
            """)
    @ResultMap("m1")

    List<Task> getAllTasksForCurrentUser(@Param("currentUserId") Integer currentUserId, boolean asc, boolean desc, Integer page, Integer size);


    @Select("""
            select * from task_tb where user_id = #{currentUserId} AND id = #{taskId}
            """)
    @ResultMap("m1")
    Task getTaskByIdForCurrentUser(@Param("taskId") Integer taskId, @Param("currentUserId") Integer userId);


    @Select("""
            delete from task_tb where id = #{taskId} AND user_id = #{currentUserId} returning *
            """)
    @ResultMap("m1")
    Task deleteTaskByIdForCurrent(@Param("taskId") Integer taskId, @Param("currentUserId") Integer currentUserId);






    @Select("""
            update task_tb set name = #{task.taskName}, description = #{task.description}, date = #{task.date}, status = #{task.status}, category_id = #{task.categoryId} where id = #{taskId} and user_id = #{userId} returning *
            """)
    @ResultMap("m1")
    Task updateTaskbyIdForCurrentUser(@Param("task") TaskRequest taskRequest, @Param("taskId") Integer taskId, @Param("userId") Integer userId);
}
