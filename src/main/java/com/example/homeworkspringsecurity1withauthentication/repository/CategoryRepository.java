package com.example.homeworkspringsecurity1withauthentication.repository;

import com.example.homeworkspringsecurity1withauthentication.model.categoryModel.Category;
import com.example.homeworkspringsecurity1withauthentication.model.taskModel.Task;
import com.example.homeworkspringsecurity1withauthentication.model.categoryModel.CategoryRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {
    @Select("INSERT INTO category_tb (name,user_id) VALUES(#{request.categoryName},#{userId}) "+
            "RETURNING id")
    Integer saveCategory(@Param("request") CategoryRequest categoryRequest,Integer userId);

    @Select("SELECT * FROM category_tb ORDER BY id ")
    @Results(
            id = "c1",
            value = {
                    @Result(property = "categoryId", column = "id"),
                    @Result(property = "categoryName", column = "name"),
                    @Result(property = "timestamp", column = "date"),
                    @Result(property = "userId", column = "user_id")
            }
    )
    Category save(@Param("cate") Category category);


    @Select("""
            select id from category_tb
            """)
    List<Integer> getAllCategoryIdExistInApp();





    @Select("SELECT * FROM category_tb ORDER BY id ASC")
    @ResultMap("c1")
    List<Category> findAllCategory();

    @Select("SELECT * FROM category_tb WHERE id = #{categoryId}")
    @ResultMap("c1")
    Category getCategoryById(Integer categoryId);

    @Select("SELECT * FROM category_tb WHERE user_id = #{userId} ORDER BY " +
            "CASE WHEN #{asc} THEN date END ASC," +
            "CASE WHEN #{desc} THEN date END DESC " +
            "LIMIT #{size} OFFSET #{size} * (#{page} - 1)"
    )
    @ResultMap("c1")
    List<Category> getAllCategoryForCurrentUser(Integer userId, boolean asc, boolean desc, Integer page, Integer size);

    @Select("SELECT * FROM category_tb WHERE id = #{categoryId} AND user_id = #{userId}")
    @ResultMap("c1")
    Category getCategoryByIdForCurrentUser(Integer categoryId, Integer userId);
    @Delete("DELETE FROM category_tb WHERE id = #{categoryId} AND user_id = #{userId}")
    @ResultMap("c1")
    boolean deleteCategoryByIdForCurrentUser(Integer categoryId, Integer userId);

    @Select("UPDATE category_tb " +
            "SET name = #{request.categoryName}" +
            " WHERE id = #{categoryId} AND user_id = #{userId}"+
            " RETURNING id")
    Integer updateCategory(@Param("request") CategoryRequest categoryRequest, Integer categoryId, Integer userId);


}
