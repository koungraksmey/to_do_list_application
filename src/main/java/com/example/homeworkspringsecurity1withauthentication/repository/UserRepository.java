package com.example.homeworkspringsecurity1withauthentication.repository;


import com.example.homeworkspringsecurity1withauthentication.model.UserInfo;
import com.example.homeworkspringsecurity1withauthentication.model.UserRequest;
import org.apache.ibatis.annotations.*;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface UserRepository {


//    @Select("""
//            select * from users where name = #{name}
//            """)
//    UserInfo findUserByUsername(@Param("name") String name);
//
//

//


    @Select("""
            select rt.name from role_tb rt 
            inner join user_roles_tb urt on rt.id =urt.role_id where user_id = #{userId}
            """)
    List<String> getRoleByUserId(@Param("userId") Integer userId);

    @Select("""
            select * from user_tb where email = #{email} or username = #{email}
            """)
    @Results(id = "mapUser", value = {
            @Result(property = "userId", column = "id"),
            @Result(property = "userName", column = "username"),
            @Result(property = "email", column = "email"),
            @Result(property = "password", column = "password"),
            @Result(property = "roles", column = "id",
                    many = @Many(select = "getRoleByUserId"))
    })
    UserInfo findByEmail(@Param("email") String email);


    @Select("""
            insert into user_tb(username, email, password) VALUES (#{info.userName}, #{info.email}, #{info.password}) returning * 
             """)
    @ResultMap("mapUser")
    UserInfo saveUser(@Param("info") UserInfo userInfo);


    default UserInfo saveUserMain(UserInfo userInfo){
        UserInfo userInfo1 = saveUser(userInfo);
        userInfo1.setRoles(userInfo.getRoles());
        System.out.println("userInfo1 :" + userInfo1.toString());
        saveRoleFromUserInfrom(userInfo1);
        return userInfo1;
    }



    @Insert("""
            insert into user_roles_tb (user_id, role_id) values (#{userId}, #{roleId})
            """)
     void insertRole(@Param("userId") int userId, @Param("roleId") int roleId);


    default void saveRoleFromUserInfrom(UserInfo userInfo) {
        int id = userInfo.getUserId();
        System.out.println(userInfo);
        System.out.println("id: "+ id);
        List<String> roles = userInfo.getRoles();

        System.out.println("role: " + roles);
        for (int i = 0; i < roles.size(); i++){
//            System.out.println("r: " + r);
            if(roles.get(i).equalsIgnoreCase("ROLE_USER")){
                insertRole(id, 1);

            } else if (roles.get(i).equalsIgnoreCase("ROLE_ADMIN")) {
                insertRole(id, 2);

            }
        }

    }

}


