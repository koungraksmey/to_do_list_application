package com.example.homeworkspringsecurity1withauthentication.config;


import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AllConfig {
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
