package com.example.homeworkspringsecurity1withauthentication.exception.taskexception;

public class YouAreNotOwnerException extends Exception{
    public YouAreNotOwnerException(String message) {
        super(message);
    }
}
