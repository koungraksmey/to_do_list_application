package com.example.homeworkspringsecurity1withauthentication.exception.taskexception;


import com.example.homeworkspringsecurity1withauthentication.exceptionCategory.CheckException;
import com.example.homeworkspringsecurity1withauthentication.exceptionCategory.IdNotFoundException;
import com.example.homeworkspringsecurity1withauthentication.model.taskModel.TaskConstant;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.net.URI;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public Map<String, Object> handleException(ResourceNotFoundException ex){
        Map<String, Object> errors = new LinkedCaseInsensitiveMap<>();
        errors.put("errorMessage", ex.getMessage());
        errors.put("date", new Timestamp(System.currentTimeMillis()));
        return errors;
    }



    @ExceptionHandler(WrongFormatInputTaskException.class)
    public Map<String, Object> handleInputWrongTaskStatusException(WrongFormatInputTaskException ex){
        Map<String, Object> errors = new LinkedCaseInsensitiveMap<>();
        errors.put("errorMessage", ex.getMessage() + ". Please use " + TaskConstant.getAllTaskStatus().toString());
        errors.put("date", new Timestamp(System.currentTimeMillis()));
        return errors;
    }

    @ExceptionHandler(CategoryNotFoundException.class)
    public Map<String, Object> categoryNotFoundException(CategoryNotFoundException ex){
        Map<String, Object> errors = new LinkedCaseInsensitiveMap<>();
        errors.put("errorMessage", ex.getMessage());
        errors.put("date", new Timestamp(System.currentTimeMillis()));
        return errors;
    }


    @ExceptionHandler(YouAreNotOwnerException.class)
    public Map<String, Object> youAreNotOwnerOfTask(YouAreNotOwnerException ex) {
        Map<String, Object> errors = new LinkedCaseInsensitiveMap<>();
        errors.put("errorMessage", ex.getMessage());
        errors.put("date", new Timestamp(System.currentTimeMillis()));
        return errors;
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, Object> methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        Map<String, Object> err = new LinkedHashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(e -> err.put(e.getField(), e.getDefaultMessage()));

        return err;

    }


    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Map<String, Object> methodArgumentNotValidExceptionHandler(HttpMessageNotReadableException ex) {
        Map<String, Object> err = new LinkedCaseInsensitiveMap<>();
        err.put("title", "Incorrect request body syntax, JSON parse error: Illegal unquoted character");
        err.put("errorMessage", ex.getMessage());
        err.put("date", new Timestamp(System.currentTimeMillis()));
        err.put("status", HttpStatus.BAD_REQUEST);
        return err;
    }

    @ExceptionHandler(CheckException.class)
    ProblemDetail handleCheckException(CheckException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST,e.getMessage());
        problemDetail.setTitle("Error!!!");
        problemDetail.setType(URI.create("http:localhost:8080/errors/not-found"));
        problemDetail.setProperty("timestamp", Instant.now());
        return problemDetail;
    }

    @ExceptionHandler(IdNotFoundException.class)
    ProblemDetail handleIdNotFound(IdNotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST,e.getMessage());
        problemDetail.setTitle("Error!!!");
        problemDetail.setType(URI.create("http:localhost:8080/errors/not-found"));
        problemDetail.setProperty("timestamp", Instant.now());
        return problemDetail;
    }
}
