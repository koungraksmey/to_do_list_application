package com.example.homeworkspringsecurity1withauthentication.exception.taskexception;

public class WrongFormatInputTaskException extends Exception{

    public WrongFormatInputTaskException(String message) {
        super(message);
    }
}
