package com.example.homeworkspringsecurity1withauthentication.exception.taskexception;

public class CategoryNotFoundException extends Exception{
    public CategoryNotFoundException(String message) {
        super(message);
    }
}
